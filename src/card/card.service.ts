import { CardDtoWithID } from './dto/idcard.dto ';
import { ScannedCardsEntity } from './scanned.cards.entity';

import { Injectable } from '@nestjs/common';
import {Repository} from 'typeorm';
import {InjectRepository} from '@nestjs/typeorm';
import {CardEntity} from './card.entity';
import {CardDto} from './dto/card.dto';

@Injectable()
export class CardService {

    constructor(
        @InjectRepository(CardEntity)
        private cardRep: Repository<CardEntity>,
        @InjectRepository(ScannedCardsEntity)
        private scanRep: Repository<ScannedCardsEntity>,
    ) {}

    async createCard(user: CardDto) {
        return await this.cardRep.save(user);
    }

    async saveScannedCard(userID: string, cardID: number) {
        const link: ScannedCardsEntity = new ScannedCardsEntity();
        link.card = cardID;
        link.user = userID;
        return await this.scanRep.save(link);
    }

    async findOne(id: number) {
        return await this.cardRep.findOne({where: {id}});
    }

    async updateCard(c: CardDtoWithID) {
        let card: CardEntity = await this.findOne(c.id);
        let old = card;
        old.id = null;
        old.active = 0;
        old = await this.cardRep.save(old);
        card = c;
        card.prevVersionID = old.id;
        return await this.cardRep.save(card);
    }

    async getHistoryOfCard(id: number) {
        const cards: CardDtoWithID[] = [];
        let c = await this.findOne(id);
        cards.push(c);
        while (c.prevVersionID != null) {
            c = await this.findOne(c.prevVersionID);
            cards.push(c);
        }
        return cards;
    }

    async findAllForUser(id: string): Promise<CardEntity[] | undefined> {
        return await this.cardRep.find({where: {userID: id, active: 1}});
    }


    async findAllScannedFromUser(id: string): Promise<ScannedCardsEntity[] | undefined> {
        return await this.scanRep.find({where: {userID: id}});
    }
}
