
export class CardDtoWithID {

    public company: string;
    public userID: string;
    public catchLine: string;
    public tittle: string;
    public name: string;
    public surname: string;
    public email: string;
    public landingPage: string;
    public phone: string;
    public address: string;
    public designURL: string;
    public instagram: string;
    public facebook: string;
    public linkedIn: string;
    public twitter: string;
    public prevVersionID: number;
    public active: number;
    public id: number;

}
