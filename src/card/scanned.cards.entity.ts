import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'scannedCards' })
export class ScannedCardsEntity {

    @PrimaryGeneratedColumn()
    public id: number;

    @Column({nullable: false})
    public user: string;

    @Column({nullable: false})
    public card: number;

}
