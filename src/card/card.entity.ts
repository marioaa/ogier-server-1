import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'card' })
export class CardEntity {

    @PrimaryGeneratedColumn()
    public id: number;

    @Column({nullable: false})
    public userID: string;

    @Column({nullable: false})
    public company: string;

    @Column({nullable: false})
    public catchLine: string;

    @Column({nullable: false})
    public tittle: string;

    @Column()
    public name: string;

    @Column()
    public surname: string;

    @Column()
    public email: string;

    @Column()
    public landingPage: string;

    @Column()
    public phone: string;

    @Column()
    public address: string;

    @Column()
    public designURL: string;

    @Column()
    public instagram: string;

    @Column()
    public facebook: string;

    @Column()
    public linkedIn: string;

    @Column()
    public twitter: string;

    @Column({nullable: true})
    public prevVersionID: number;

    @Column()
    public active: number;  // 1 for active 0 for inactive

}
