import { CardDtoWithID } from './dto/idcard.dto ';
import { CardDto } from './dto/card.dto';
import { CardService } from './card.service';
import {Controller, Get, Request, Post, UseGuards, Body, Param} from '@nestjs/common';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';

@Controller('card')
export class CardController {
  constructor(private service: CardService) {}

  @UseGuards(JwtAuthGuard)
  @Post('create/')
  async createCArd(@Body() req: CardDto) {
    return this.service.createCard(req);
  }

  @UseGuards(JwtAuthGuard)
  @Post('update/')
  async update(@Body() req: CardDtoWithID) {
    return this.service.updateCard(req);
  }

  @UseGuards(JwtAuthGuard)
  @Get('scanned/now/:id')
  saveScannedCard(@Request() req: any, @Param() params) {
    return this.service.saveScannedCard(req.user.username, params.id);
  }

  @UseGuards(JwtAuthGuard)
  @Get('give/me/:id')
  getMyCardWith(@Request() req: any, @Param() params) {
    return this.service.findOne(params.id);
  }

  @UseGuards(JwtAuthGuard)
  @Get('history/of/:id')
  getHistory(@Param() params) {
    return this.service.getHistoryOfCard(params.id);
  }

  @UseGuards(JwtAuthGuard)
  @Get('scanned/all/')
  getAllScannedCards(@Request() req) {
    return this.service.findAllScannedFromUser(req.user.username);
  }

  @UseGuards(JwtAuthGuard)
  @Get('mine/')
  async getAllMyCards(@Request() req) {
    return {
      myCards: await this.service.findAllForUser(req.user.username),
      scannedCards: await this.service.findAllScannedFromUser(req.user.username),
    };
  }

  @Get('/')
  sayHi(@Request() req) {
    return 'card controller';
  }

}
