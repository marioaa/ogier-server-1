import { ScannedCardsEntity } from './scanned.cards.entity';
import { CardController } from './card.controller';
import { Module, Controller, HttpModule } from '@nestjs/common';
import {TypeOrmModule} from '@nestjs/typeorm';
import {CardEntity} from './card.entity';
import {CardService} from './card.service';

@Module({
  imports: [TypeOrmModule.forFeature([CardEntity, ScannedCardsEntity])],
  controllers: [CardController],
  providers: [CardService],
  exports: [CardService],
})
export class CardModule {}
