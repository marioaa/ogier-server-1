
export class UserDto {

    public username: string;
    public password: string;
    public email: string;
    public name: string;
    public surname: string;
    public role: number;
}
