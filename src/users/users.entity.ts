import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'user' })
export class UsersEntity {

    @PrimaryGeneratedColumn()
    public id: number;

    @Column({ unique: true, nullable: false})
    public username: string;

    @Column()
    public password: string;

    @Column()
    public name: string;

    @Column()
    public surname: string;

    @Column()
    public email: string;

    @Column()
    public role: number;

}
