import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'telegramUsers' })
export class TelEntity {

  @PrimaryGeneratedColumn()
  public id: string;

  @Column({unique: true, nullable: false })
  public telegramUserID: number;

  @Column({ nullable: false })
  public role: number;

  @Column({ nullable: false })
  public description: string;
}
