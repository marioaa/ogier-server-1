import { TelEntity } from './telegramUsers.entity';
import { Module, HttpModule } from '@nestjs/common';
import { TelegramService } from './telegram.service';
import { TelegramController } from './telegram.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ScheduleModule } from '@nestjs/schedule';

@Module({
  imports: [TypeOrmModule.forFeature([TelEntity]), HttpModule, ScheduleModule.forRoot()],
  providers: [TelegramService],
  controllers: [TelegramController],
})
export class TelegramModule {}
