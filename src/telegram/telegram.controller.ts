import { MSG } from './dto/msg.dto';
import { TelegramService } from './telegram.service';
import { Controller, UseGuards, Get, Req, Body, Post } from '@nestjs/common';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';

@Controller('telegram')
export class TelegramController {
  constructor(private service: TelegramService) { }

  @UseGuards(JwtAuthGuard)
  @Get('new/purchase/')
  async sendPurchasedMsg(@Req() req: any) {
    return this.service.notifyPurchase(req);
  }

  @UseGuards(JwtAuthGuard)
  @Post('send/msg/')
  async sendMsg(@Body() req: MSG) {
    return this.service.sendMsgToAdm(req);
  }
  @UseGuards(JwtAuthGuard)
  @Post('set/webhook')
  async webhook(@Body() req: any) {
    return this.service.webHook(req);
  }
  @UseGuards(JwtAuthGuard)
  @Post('send/msg/to/printer')
  async toPrinter(@Body() req: MSG) {
    return this.service.sendMsgToPrinter(req);
  }

}
