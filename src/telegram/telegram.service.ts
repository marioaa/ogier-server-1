import { MSG } from './dto/msg.dto';
import { TelEntity } from './telegramUsers.entity';
import { Injectable, HttpService } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Cron } from '@nestjs/schedule';

@Injectable()
export class TelegramService {
  private print = console.log;
  private botT = '1316984210:AAEfAXhddoAH5DsTGqcmGE_FhOJoA1aruro';
  private endpoint = `https://api.telegram.org/bot${this.botT}`;
  private updateID = 370893166;

  constructor(
    @InjectRepository(TelEntity)
    private telRep: Repository<TelEntity>,
    private world: HttpService,
  ) { }

  async register(data: any) {
    return await this.telRep.save(data);
  }

  async keepCount() {
    const data = {
      telegramUserID: 0,
      role: 0,
      description: 'Keeping count of messages',
    };
    return await this.register(data);
  }
  async rmCount() {
    const count = await this.findOne(0);
    count.role = 0;
    return await this.telRep.save(count);
  }

  async updateCount(c) {
    const count = await this.findOne(0);
    count.role = c;
    return await this.telRep.save(count);
  }
  async getCurrCount() {
    const count: TelEntity = await this.findOne(0);
    if (count !== undefined) {
      return count.role;
    }
    return 0;
  }

  async findOne(id: any): Promise<TelEntity | undefined> {
    return await this.telRep.findOne({ where: { telegramUserID: id } });
  }
  async findAdm(): Promise<TelEntity[] | undefined> {
    return await this.telRep.find({ where: { role: 9 } });
  }

  async findPrinterGuy(): Promise<TelEntity | undefined> {
    return await this.telRep.findOne({ where: { role: 1 } });
  }

  async getMsg() {
    this.world.get(this.endpoint + '/getUpdates').subscribe((data) => {
      if (data.data.ok === true) {
        const msgs: any[] = data.data.result;
        msgs.forEach(async (msg) => {
          const i = await this.getCurrCount();
          if (i !== undefined && msg.update_id > i) {
            await this.updateCount(msg.update_id);
            msg = msg.message;
            this.print(`${msg.from.first_name} ${msg.from.last_name} sent ${msg.text}`);
            this.handleMsg(msg);
          }
        });
      }
    });
  }
  async webHook(data: any) {
    // this.world.get(this.endpoint + '/getUpdates').subscribe((data) => {
      if (data.data.ok === true) {
        const msgs: any[] = data.data.result;
        msgs.forEach(async (msg) => {
          const i = await this.getCurrCount();
          if (i !== undefined && msg.update_id > i) {
            await this.updateCount(msg.update_id);
            msg = msg.message;
            this.print(`${msg.from.first_name} ${msg.from.last_name} sent ${msg.text}`);
            this.handleMsg(msg);
          }
        });
      }
    // });
  }

  async notifyPurchase(req: any) {
    const adm: TelEntity[] = await this.findAdm();
    const msg = `${req.user.username} just purchased an OGIER CARD`;
    adm.forEach((user) => {
      this.send(user.telegramUserID, msg);
    });
  }
  async sendMsgToAdm(req: MSG) {
    const adm: TelEntity[] = await this.findAdm();
    const msg = `${req.msg}`;
    adm.forEach((user) => {
      this.send(user.telegramUserID, msg);
    });
  }
  async sendMsgToPrinter(req: MSG) {
    const adm: TelEntity[] = await this.findAdm();
    const msg = `${req.msg}`;
    adm.forEach((user) => {
      this.send(user.telegramUserID, msg);
    });
  }

  async handleMsg(msg: any) {
    if (msg.text === '/ogierole9') {
      const data = {
        telegramUserID: msg.from.id,
        role: 9,
        description: 'Administrator, the guy that gets all the notifications',
      };
      const u: TelEntity = await this.register(data);
      this.send(msg.from.id, `you were saved with role: ${u.role} description: ${u.description}`);
    }
    if (msg.text === '/ogierole3') {
      const data = {
        telegramUserID: msg.from.id,
        role: 3,
        description: 'Printer Guy, the guy that prints all the cards',
      };
      const u: TelEntity = await this.register(data);
      this.send(msg.from.id, `you were saved with role: ${u.role} description: ${u.description}`);
    }
    if (msg.text === '/rm') {
      const user = await this.findOne(msg.from.id);
      await this.telRep.remove(user);
      this.send(msg.from.id, `You were terminated`);
    }
    if (msg.text === '/keepCount') {
      this.keepCount();
      this.send(msg.from.id, `Starting to keep Count`);
    } else if (msg.text === undefined) {
      this.send(msg.from.id, `Sorry can't handle that rn 💦`);
    } else {
      this.send(msg.from.id, msg.text);
    }
  }

  async send(to: any, msg: string) {
    const data = {
      chat_id: to,
      text: msg,
    };
    this.print(data);
    this.world.post(`${this.endpoint}/sendMessage`, data).subscribe((d) => {
      this.print('message Sent', data);
    });
  }
}
