import { ServiceController } from './service.controller';
import { Module, Controller } from '@nestjs/common';
import {TypeOrmModule} from '@nestjs/typeorm';
import {ServiceEntity} from './service.entity';
import {ServiceService} from './service.service';

@Module({
  imports: [TypeOrmModule.forFeature([ServiceEntity])],
  controllers: [ServiceController],
  providers: [ServiceService],
  exports: [ServiceService],
})
export class ServiceModule {}
