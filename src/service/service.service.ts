
import { Injectable } from '@nestjs/common';
import {Repository} from 'typeorm';
import {InjectRepository} from '@nestjs/typeorm';
import {ServiceEntity} from './service.entity';
import {ServiceDto} from './dto/service.dto';

@Injectable()
export class ServiceService {

    constructor(
        @InjectRepository(ServiceEntity)
        private serviceRep: Repository<ServiceEntity>,
    ) {}

    async saveService(service: ServiceDto) {
        return await this.serviceRep.save(service);
    }

    async findOne(cardID: number): Promise<ServiceEntity[] | undefined> {
        return await this.serviceRep.find({where: {cardID}});
    }

    async findAll(): Promise<ServiceEntity[] | undefined> {
        return await this.serviceRep.find();

    }
}
