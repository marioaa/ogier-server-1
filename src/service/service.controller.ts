import { ServiceDto } from './dto/service.dto';
import { ServiceService } from './service.service';
import {Controller, Get, Request, Post, UseGuards, Body, Param} from '@nestjs/common';
import { LocalAuthGuard } from 'src/auth/local-auth.guard';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';

@Controller('service')
export class ServiceController {
  constructor(private services: ServiceService) {}

  @UseGuards(JwtAuthGuard)
  @Post('add/')
  async login(@Body() req: ServiceDto) {
    return this.services.saveService(req);
  }

  @UseGuards(JwtAuthGuard)
  @Get('all')
  getProfile(@Request() req) {
    return this.services.findAll();
  }

  @UseGuards(JwtAuthGuard)
  @Get('fromCard/:id')
  getServicesFromCard(@Param() params) {
    return this.services.findOne(params.id);
  }

  @Get('/')
  sayHi(@Request() req) {
    return 'service thingy';
  }

}
