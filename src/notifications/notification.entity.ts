import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'service' })
export class NotificationEntity {

    @PrimaryGeneratedColumn()
    public id: number;

    @Column({nullable: false})
    public cardID: number;

    @Column()
    public title: string;

    @Column()
    public description: string;

    @Column()
    public photoURL: string;

    @Column()
    public currencyChar: string;

    @Column()
    public price: number;

}
