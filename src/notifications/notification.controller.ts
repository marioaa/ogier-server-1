import { NotificationDto } from './dto/notification.dto';
import { NotificationService } from './notification.service';
import {Controller, Get, Request, Post, UseGuards, Body, Param} from '@nestjs/common';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';

@Controller('service')
export class NotificationController {
  constructor(private service: NotificationService) {}

  @UseGuards(JwtAuthGuard)
  @Post('add/')
  async login(@Body() req: NotificationDto) {
    return this.service.saveService(req);
  }

  @UseGuards(JwtAuthGuard)
  @Get('all')
  getProfile(@Request() req) {
    return this.service.findAll();
  }

  @UseGuards(JwtAuthGuard)
  @Get('fromCard/:id')
  getServicesFromCard(@Param() params) {
    return this.service.findOne(params.id);
  }

  @Get('/')
  sayHi(@Request() req) {
    return 'service thingy';
  }

}
