import { NotificationDto } from './dto/notification.dto';
import { NotificationEntity } from './notification.entity';
import { Injectable } from '@nestjs/common';
import {Repository} from 'typeorm';
import {InjectRepository} from '@nestjs/typeorm';

@Injectable()
export class NotificationService {

    constructor(
        @InjectRepository(NotificationEntity)
        private notificationRep: Repository<NotificationEntity>,
    ) {}

    async saveService(service: NotificationDto) {
        return await this.notificationRep.save(service);
    }

    async findOne(cardID: number): Promise<NotificationEntity[] | undefined> {
        return await this.notificationRep.find({where: {cardID}});
    }

    async findAll(): Promise<NotificationEntity[] | undefined> {
        return await this.notificationRep.find();

    }
}
