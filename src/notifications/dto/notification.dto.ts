
export class NotificationDto {

    public cardID: number;
    public title: string;
    public description: string;
    public photoURL: string;
    public currencyChar: string;
    public price: number;

}
